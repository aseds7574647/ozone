const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const axios = require('axios');
const fs = require('fs');
const csv = require('csv-parser');

const app = express();
const port = 3000;

// Connexion à MongoDB
mongoose.connect('mongodb://localhost:27017/ozone');

// Schéma MongoDB
const gasDataSchema = new mongoose.Schema({
  ppm: Number,
  temperature: Number,
  prediction: [Number],
  timestamp: { type: Date, default: Date.now }
});

const GasData = mongoose.model('GasData', gasDataSchema);

// Middleware pour analyser le corps des requêtes
app.use(bodyParser.json());

// Point de terminaison POST pour recevoir les données du capteur de gaz
app.post('/post', async (req, res) => {
  try {
    // Faire une prédiction avec les données reçue
    const response = await axios.post(`http://localhost:5000/predict`, {ppm: req.body.ppm, temp: req.body.temp})
    // Créer une nouvelle instance de GasData avec les données du corps de la requête
    const newGasData = new GasData({
      ppm: req.body.ppm,
      temperature: req.body.temp,
      prediction: response.data.normalized_data[0]
    });

    // Sauvegarder dans la base de données MongoDB
    const savedGasData = await newGasData.save();
    if (savedGasData){

    }

    console.log('Données de capteur de gaz enregistrées dans MongoDB :', savedGasData);
    res.status(200).send('Données enregistrées avec succès');
  } catch (error) {
    console.error('Erreur lors de l\'enregistrement des données dans MongoDB :', error);
    res.status(500).send('Erreur lors de l\'enregistrement des données');
  }
});

// Endpoint GET pour récupérer toutes les données de la base de données
app.get('/', async (req, res) => {
    try {
      // Récupérer toutes les données de la base de données
      const allGasData = await GasData.find();
  
      res.status(200).json(allGasData);
    } catch (error) {
      console.error('Erreur lors de la récupération des données depuis MongoDB :', error);
      res.status(500).send('Erreur lors de la récupération des données');
    }
});

app.get('/getPrediction', async (req, res) => {
  try {
    // récupérer la dérnière valeur enregistrée
    const lastPred = await GasData.findOne().sort({ _id: -1 }).limit(1);

    // renvoyer la valeur trouver en reponse
    console.log("Données envoyées au frontend")
    res.json({ pred: lastPred ? lastPred : null });
  } catch (error) {
    console.error('Erreur lors de la récupération de la dernière valeur :', error);
    res.status(500).json({ erreur: 'Erreur serveur' });
  }
})

// Endpoint pour ajouter des données au fichier CSV
app.post('/csv', (req, res) => {
  const nouvellesDonnees = req.body;

  // Chemin vers le fichier CSV
  const cheminFichierCSV = 'C:/Users/PROBOOK/OneDrive/Documents/ASEDS_3/Projet d\'ouverture/ozone/ozone.csv';

  // Lire les données existantes dans le fichier CSV
  const donneesExistantes = [];

  fs.createReadStream(cheminFichierCSV)
      .pipe(csv())
      .on('data', (row) => {
        donneesExistantes.push(row);
      })
      .on('end', () => {
        // Avant d'ajouter les nouvelles données
        if (nouvellesDonnees && Object.values(nouvellesDonnees).every((value) => value !== null && value !== undefined)) {
          donneesExistantes.push(nouvellesDonnees);
        } else {
          // Gérer le cas où les nouvelles données sont null
          res.status(400).send('Les nouvelles données sont invalides.');
          return;
        }


        // Écrire les données mises à jour dans le fichier CSV
        const fichierCSVStream = fs.createWriteStream(cheminFichierCSV);
        fichierCSVStream.write(Object.keys(donneesExistantes[0]).join(',') + '\n');
        donneesExistantes.forEach((row) => {
          // Remplacez les valeurs null par une chaîne vide (ou une autre valeur par défaut)
          const cleanedRow = Object.fromEntries(Object.entries(row).map(([key, value]) => [key, value || '']));
          fichierCSVStream.write(Object.values(cleanedRow).join(',') + '\n');
        });
        fichierCSVStream.end();

        res.send('Données ajoutées avec succès !');
      });
});



  
  const db = mongoose.connection;

  db.on('error', (error) => {
    console.error('Erreur de connexion à MongoDB :', error);
  });
  
  db.once('open', () => {
    console.log('Connecté à MongoDB avec succès');
  });
  

// Démarrer le serveur
app.listen(port, () => {
  console.log(`Serveur démarré sur le port ${port}`);
});