from flask import Flask, jsonify, request
import numpy as np
from tensorflow.keras.models import load_model
from datetime import datetime, timedelta
from matplotlib import pyplot as plt
import pandas as pd
import json

# Initialisation de Flask et Socket.IO
app = Flask(__name__)

model_file_path = 'C:/Users/PROBOOK/OneDrive/Documents/ASEDS_3/Projet d\'ouverture/ozone/backend/ia/best_model.h5'
model = load_model(model_file_path)

# Charger les moyennes et les écarts types depuis le fichier JSON
with open('C:/Users/PROBOOK/OneDrive/Documents/ASEDS_3/Projet d\'ouverture/ozone/backend/ia/scaling_info.json',
          'r') as json_file:
    scaling_info = json.load(json_file)

# Convertir les dictionnaires en pandas Series et assurer que les valeurs sont de type float
mean, std = [pd.Series(s, dtype=float) for s in scaling_info]

# ---- About dataset (no need to change)

features = ['CO2_ppm', 'Temperature_C']

# ---- About training (Can be changed !)

sequence_len = 30


def plot_multivariate_serie(sequence_true, predictions, labels, only_features=None, width=14, height=8, save_as=None):
    plt.figure(figsize=(width, height))

    # S'il n'y a qu'une seule caractéristique à afficher
    if only_features is not None:
        feat = only_features[0]
        plt.plot(sequence_true[:, feat], label=f'Real {labels[feat]}', marker='o')
        plt.plot(np.arange(len(sequence_true), len(sequence_true) + len(predictions)), predictions[:, feat],
                 label=f'Predicted {labels[feat]}', marker='x')

    # S'il y a plusieurs caractéristiques à afficher
    else:
        for feat in range(len(labels)):
            plt.plot(sequence_true[:, feat], label=f'Real {labels[feat]}', marker='o')
            plt.plot(np.arange(len(sequence_true), len(sequence_true) + len(predictions)), predictions[:, feat],
                     label=f'Predicted {labels[feat]}', marker='x')

    plt.xlabel('Time Step')
    plt.ylabel('Value')
    plt.legend()
    plt.title('Multivariate Time Series Prediction')

    if save_as:
        plt.savefig(f'{save_as}.png')

    plt.show()


def data_generator(num_minutes=2000, resample_freq='5T', threshold_co2=25):
    # Générer une graine aléatoire unique à chaque exécution
    seed = np.random.randint(0, 1000000)
    np.random.seed(seed)

    # Générer une séquence de dates à intervalles d'une minute
    start_date = datetime(2012, 1, 1)
    end_date = start_date + timedelta(minutes=num_minutes)
    date_sequence = pd.date_range(start=start_date, end=end_date, freq='T')

    # Générer des données pour le taux de CO2 avec une distribution normale tronquée
    mean_co2 = 600
    std_dev_co2 = 50
    lower_limit_co2 = 450
    upper_limit_co2 = 1000
    co2_values = np.clip(np.random.normal(mean_co2, std_dev_co2, len(date_sequence)), lower_limit_co2, upper_limit_co2)

    # Générer des données pour l'humidité avec une distribution normale tronquée
    mean_humidity = 60
    std_dev_humidity = 20
    lower_limit_humidity = 0
    upper_limit_humidity = 100
    humidity_values = np.clip(np.random.normal(mean_humidity, std_dev_humidity, len(date_sequence)),
                              lower_limit_humidity, upper_limit_humidity)

    # Générer des données pour la température
    temperature_values = np.random.uniform(18, 30, len(date_sequence))

    # Créer un DataFrame avec des données factices
    data = {
        'Date': date_sequence,
        'CO2_ppm': co2_values,
        'Temperature_C': temperature_values,
        'Humidity_percent': humidity_values
    }

    df = pd.DataFrame(data)

    # Ajouter une colonne heure du jour
    df['Hour'] = df['Date'].dt.hour

    # Tri par date
    df = df.sort_values(by='Date')

    # Réinitialiser les index
    df = df.reset_index(drop=True)

    # Rééchantillonner les données pour toutes les caractéristiques
    df_resampled = df.set_index('Date').resample(resample_freq).mean()

    # Filtrer les points où la différence avec le précédent est supérieure à threshold_co2 pour le CO2
    sampled_data = df_resampled[df_resampled['CO2_ppm'].diff().abs() < threshold_co2]

    # retourner les données
    return sampled_data


# ... obtenir les données à normaliser
data = data_generator()

# Réinitialiser l'index
data = data.reset_index(drop=True)


def normalize_data(ppm, temp, data):
    # Ajouter une nouvelle ligne
    new_row = {"CO2_ppm": ppm, "Temperature_C": temp}
    data.loc[len(data)] = new_row

    # Récupérer les 30 dernières lignes
    data = data.tail(sequence_len)

    # Normaliser les données en utilisant les moyennes et les écarts types
    normalized_data = (data[features] - mean) / std

    normalized_data = normalized_data.to_numpy()

    # Retourner les données normalisées
    return normalized_data


def denormalize(mean, std, seq):
    nseq = seq.copy()
    for i, s in enumerate(nseq):
        s = s * std + mean
        nseq[i] = s
    return nseq


@app.route('/predict', methods=['POST'])
def predict_co2():
    if request.is_json:
        # ---- Get a sequence
        df = request.get_json()
        dataset_test = normalize_data(float(df["ppm"]), float(df["temp"]), data)
        sequence = dataset_test[-sequence_len:]
        sequence_true = dataset_test[-(sequence_len + 1):]

        # ---- Prediction
        pred = model.predict(np.array([sequence]))

        # ---- De-normalization
        sequence_true = denormalize(mean, std, sequence_true)
        pred = denormalize(mean, std, pred)

        # Utilisation de la fonction
        feat_to_plot = 0  # Caractéristique à afficher
        plot_multivariate_serie(sequence_true, pred, labels=features, only_features=[feat_to_plot], width=14, height=8,
                                save_as='03-prediction')

        # Calcul de l'écart entre la prédiction et la réalité
        delta_deg = np.abs(sequence_true[-1][feat_to_plot] - pred[-1][feat_to_plot])
        print(f'\nGap between prediction and reality : {delta_deg:.2f} °C')

        # Retourner les données normalisées en tant que réponse JSON
        return jsonify({'normalized_data': pred.tolist(),
                        'message': f'Gap between prediction and reality : {delta_deg:.2f} °C'
                        })
    else:
        # Si la requête ne contient pas de données JSON, retournez une erreur
        return jsonify({"erreur": "La requête ne contient pas de données JSON"}), 400


if __name__ == '__main__':
    app.run()
