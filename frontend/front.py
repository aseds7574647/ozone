import streamlit as st
import requests
import time

# Créez une session_state pour stocker des variables entre les sessions
if 'acc' not in st.session_state:
    st.session_state.acc = []

if 'history' not in st.session_state:
    st.session_state.history = []
    st.session_state.temp = []
    st.session_state.ppm = []

# Adresse du backend Flask
api_url = 'http://localhost:3000/getPrediction'

# Intervalle de rafraichissement de la page
refresh_interval = 150


# Fonction pour obtenir les données prédites depuis le backend
def get_prediction_data():
    # Send the POST request with the request body
    headers = {'Content-Type': 'application/json'}
    response = requests.request(method='get', url=api_url, headers=headers)

    if response.status_code == 200:
        return response.json()
    else:
        return None


# Main
while True:
    # Obtenez les données prédites
    pred_data = get_prediction_data()

    # Convertir les données JSON en un format utilisable
    prediction = pred_data["pred"]["prediction"]

    # Récupérations des anciennes données stockées dans les variables réutilisables entre les différentes sessions
    history = st.session_state.history
    temperature = st.session_state.temp
    co2_rate = st.session_state.ppm

    # Sauvegarder les données dans les variables réutilisables entre les différentes sessions
    history.append(prediction)
    temperature.append(prediction[1])
    co2_rate.append(prediction[0])

    # Définir le filtre
    filtre = st.sidebar.selectbox(
        'Filter',
        ("None", "Temperature", "CO2 rate")
    )

    # Afficher les données filtrées
    st.title("Courbe d'évolution")
    if filtre == "None":
        st.line_chart(history[-20:])
    elif filtre == "Temperature":
        st.subheader("Température")
        st.line_chart(temperature[-20:], color='#f5ea00')
    elif filtre == "CO2 rate":
        st.subheader("Taux ppm de CO2")
        st.line_chart(co2_rate[-20:], color='#ffaa00')

    # Ajouter une ligne de séparation
    # st.divider()

    st.title("Obseration")
    st.sidebar.subheader("CO2 rate:     " + str(pred_data["pred"]["ppm"]) + " ppm")
    st.sidebar.subheader("Temperature:  " + str(pred_data["pred"]["temperature"]) + " °C")
    if float(pred_data["pred"]["ppm"]) <= 700:
        st.subheader("Qualité de l'air acceptable")
    elif float(pred_data["pred"]["ppm"]) <= 900:
        st.subheader("Veuillez aérer la pièce !")
    else:
        st.subheader("Seuil critique dépassé !")

    time.sleep(refresh_interval)
    st.experimental_rerun()
# Début de la seconde partie de la page
