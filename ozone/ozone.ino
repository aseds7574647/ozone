#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClient.h>
#include "MQ135.h"
#include <LiquidCrystal_I2C.h>
#include <DHT.h>

// Définir le modèle du capteur (DHT22 pour le DHT22)
#define DHT_MODEL 22

// Définir la broche à laquelle le capteur est connecté
#define DHT_PIN D3  // Remplacez par le numéro de votre broche

// Initialiser le capteur
DHT dht(DHT_PIN, DHT_MODEL);

#define RZERO 1

WiFiClient client;
HTTPClient http;

const char* ssid = "Ehvi";  // Remplacez par le nom de votre réseau WiFi
const char* password = "ehvi1234";  // Remplacez par le mot de passe de votre réseau WiFi
const char* serverAddress = "192.168.43.63";  // Remplacez par l'adresse IP de votre serveur
const int serverPort = 3000;  // Port du serveur HTTP

MQ135 gasSensor = MQ135(A0, 78.29);
int val; 
const int sensorPin = A0; // Broche analogique où est connecté le capteur de gaz
int sensorValue = 0;
const int seuil = 375;   // Valeur seuil pour déclencher l'envoi des données

int LedRed = D7;
int LedYel = D6;
int LedGre = D5;

int buzzer = D8;

LiquidCrystal_I2C lcd(0x27, 20, 4);

struct Mesure {
    int ppm;
    int temp;
};

Mesure  mesure[30];
int somme_gaz = 0;
int somme_temp = 0;
int count=0;

int gasValue = 500;

void setup() {
  Serial.begin(9600);
  pinMode(sensorPin, INPUT);

  pinMode(LedRed, OUTPUT);
  pinMode(LedYel, OUTPUT);
  pinMode(LedGre, OUTPUT);

  pinMode(buzzer, OUTPUT);

  dht.begin();

  // Connexion au réseau WiFi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(2000);
    Serial.println("Connexion au WiFi en cours...");
  }
  Serial.println("Connecté au WiFi");

  // Vous pouvez également imprimer l'adresse IP attribuée à l'ESP8266
  Serial.println(WiFi.localIP());

  // Initialiser l'écran LCD
  lcd.begin(16, 2);
  lcd.backlight(); // Activer le rétroéclairage (si disponible)

  // Afficher un message de bienvenue au démarrage
  lcd.clear();
  lcd.setCursor(0, 1);
  lcd.print("BIENVENUE");
  lcd.setCursor(0, 0);
  lcd.print("demarage du programme...");
  digitalWrite(LedGre, HIGH);
}

void loop() {
  // Lecture de la valeur analogique du capteur de gaz
  float temperature = dht.readTemperature();

  val = analogRead(A0);  
  float zero = gasSensor.getRZero(); 
  float ppm = gasSensor.getPPM();

  Serial.println("+---------------------------------------+");
  Serial.println("| raw     | rzero   | ppm     | temp    |");
  Serial.println("+---------------------------------------+");
  Serial.println("| " + String(val) + "      | " + String(zero) + "  | " + String(ppm) + "  | " + String(temperature) + "   |");
  Serial.println("+---------------------------------------+");

  // Envoyer les données pour la constitution du fichier d'entrainement
  to_csv(ppm);

  // Envoyer les données au serveur si la valeur du capteur dépasse le seuil
  if (ppm > seuil) {
    mesure[count].ppm = ppm;
    mesure[count].temp = temperature;
    count++;
    Serial.println("count: " + String(count));
    if (count == 30){
      count = 0;
      for (int i=0; i < 30; i++){
        somme_gaz += mesure[i].ppm;
        somme_temp += mesure[i].temp;
      }

      gasValue = somme_gaz/30;
      int tempValue = somme_temp/30;
      somme_gaz = somme_temp = 0;

      // Affichage sur l'écrand lcd
      lcd.clear();
      lcd.setCursor(1, 1);
      lcd.print("CO2_ppm: " + String(gasValue));
      lcd.setCursor(1, 2);
      lcd.print("Temp_C: " + String(tempValue));
 
      String postData = "{\"ppm\":" + String(gasValue) + ", \"temp\":" + String(tempValue) + "}";
      String url = "http://" + String(serverAddress) + ":" + String(serverPort) + "/post";  // Remplacez par votre URL d'envoi de données
      http.begin(client, url);
      http.addHeader("Content-Type", "application/json");
      int httpCode = http.POST(postData);
      if (httpCode == 200) {
        Serial.println("Données envoyées avec succès");
      } else {
        Serial.println("Échec de l'envoi des données");
      }

      http.end();
    }
  }
  // Faire l'affichage des led
  if(gasValue <= 700){
    turnOnLed(0);
  }
  else if(gasValue <= 900){
    turnOnLed(1);
    lcd.setCursor(1, 3);
    lcd.print("Mes: Veuillez Aérer la pièce");
    tone(buzzer, 1000, 500);
  }
  else{
    turnOnLed(2);
    lcd.setCursor(1, 3);
    lcd.print("Mes: Seuil critique dépassé");
    tone(buzzer, 1000, 300);
    delay(300);
    tone(buzzer, 500, 300);
    delay(300);
    tone(buzzer, 1000, 300);
    delay(300);
  }

  delay(2000);  // Attendre 2 secondes avant de vérifier à nouveau
}

void to_csv(float ppm){
  // Envoyer les données sur le serveur pour le fichier cvs
  String postData = "{\"ppm\":" + String(ppm) + "}";
  String url = "http://" + String(serverAddress) + ":" + String(serverPort) + "/csv";  // Remplacez par votre URL d'envoi de données
  http.begin(client, url);
  http.addHeader("Content-Type", "application/json");
  int httpCode = http.POST(postData);
}

void turnOnLed(int led){
  if(led == 2){
    digitalWrite(LedRed, HIGH);
    digitalWrite(LedYel, LOW);
    digitalWrite(LedGre, LOW);
  }
  else if(led == 1){
    digitalWrite(LedYel, HIGH);
    digitalWrite(LedRed, LOW);
    digitalWrite(LedGre, LOW);
  }
  else if(led == 0){
    digitalWrite(LedGre, HIGH);
    digitalWrite(LedYel, LOW);
    digitalWrite(LedRed, LOW);
  }
}